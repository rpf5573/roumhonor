<?php
/*챠일드 테마의 style.css가 적용되어지도록 해주는 함수*/
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // style for parent theme

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


/* '기부하기' 페이지 관련 함수들 시작 */
add_action('woocommerce_before_checkout_billing_form', 'custom_woocommerce_before_checkout_billing_form');
function custom_woocommerce_before_checkout_billing_form($checkout){?>
	<style>
		/*p#billing_last_name_field,
		p#billing_company_field,
		p#billing_country_field,
		p#billing_city_field,*/
		.woocommerce-page form .form-row label[for=order_comments] {display: none !important;}
		
		.woocommerce-page form .form-row-first {width: 100%;}
		
		
		div#wcdp {background: url(/wp-content/uploads/2023/08/roumhonors_logo.png), linear-gradient(180deg, #000 306px, transparent 306px); width: 100%; background-size: contain; background-position: top; background-repeat: no-repeat; padding: 30px; padding-top: 0;}
		.wcdp-body {position: relative; overflow: visible !important; background: #fff; margin-top: 190px;}
		.wcdp-body:before {position: absolute; content: "로움 아너스 클럽"; color: #898989; font-weight: bold; top: -50px; left: 5%;}
		.wcdp-body:after {position: absolute; content: ""; background: #946332; width: 90%; height: 8px; top: -8px; left: 50%; transform: translateX(-50%);}
		
		div#wcdp-step-1,
		div#wcdp-step-2,
		div#wcdp-step-3 {box-shadow: 0 5px 15px 0px #333; padding: 20px; /*margin-top: 20px;*/}
		
		.woocommerce form .form-row input.input-text,
		.woocommerce form .form-row textarea {border: unset; border-bottom: 2px solid #e9e9e9; border-radius: unset;}
		.woocommerce form .form-row input.input-text::placeholder,
		.woocommerce form .form-row textarea::placeholder {color: #b7b7b7;}
		
		#wcdp .wcdp-right, .wcdp-left,
		#wcdp .wcdp-right, .wcdp-right {color: #fff !important; background: #483d3c !important; border-radius: 20px !important; padding: 8px 10px !important;}
		
		@media (min-width: 601px){
			div#wcdp {width: calc(100% + 60px);}
		}
		
		@media (max-width: 600px){
			.elementor-element.elementor-element-69ae37d.elementor-widget__width-initial.elementor-widget.elementor-widget-text-editor {width: 90%; padding-bottom: 50px;}
		}
	</style>
	
	<script>
			jQuery(window).on('load', function(){
				jQuery('[name="order_comments"]').attr('placeholder','기부 비고');
			});
	</script>
	<?php
	return $checkout;
}

add_filter('woocommerce_billing_fields', 'custom_required_option');
function custom_required_option( $fields ){
	$unset_fields_arr = array(
							'billing_last_name',
							'billing_company',
							'billing_country',
						);
	
	for($i=0; $i<count($unset_fields_arr); $i++){
		$fields_index = $unset_fields_arr[$i];
		
		$fields[$fields_index]['required'] = false;
		unset($fields[$fields_index]);
	}
	
	$fields_arr = array(
					'billing_first_name',
					'billing_address_1',
					'billing_postcode',
					'billing_phone',
					'billing_email',
				);

	for($i=0; $i<count($fields_arr); $i++){
		$fields_index = $fields_arr[$i];
		
		$fields[$fields_index]['placeholder'] = $fields[$fields_index]['label'];
		$fields[$fields_index]['label'] = '';
	}
	
	return $fields;
}

add_filter('woocommerce_default_address_fields', 'custom_override_default_address_fields');
function custom_override_default_address_fields($address_fields){
	$address_fields['city']['required'] = false;
	unset($address_fields['city']);
	
	$address_fields['address_2']['placeholder'] = '상세주소';
	
	return $address_fields;
}
/* '기부하기' 페이지 관련 함수들 끝 */


function roum_get_image_url($file_path) {
	return get_stylesheet_directory_uri() . '/assets/images/' . $file_path;
}

// 회원가입 폼 수정
add_filter('wpmem_register_form_rows', 'transform_wp_members_form_structure', 10, 2);
function transform_wp_members_form_structure($rows, $toggle) {
	// 라벨 먼저 지우기
	$rows['user_email']['label'] = "";
	$rows['password']['label'] = "";
	$rows['first_name']['label'] = "";
	$rows['billing_phone']['label'] = "";
	$rows['billing_address_1']['label'] = "";
	$rows['birthday']['label'] = "";
	$rows['sex']['label'] = "";
	$rows['group_name']['label'] = "";
	$rows['team_select']['label'] = "";
	$rows['reg_levle']['label'] = "";
	$rows['recomend']['label'] = "";
	$rows['ambassaord_reg']['label'] = "";

	// email
	$rows['user_email']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/email.png'));
	$rows['user_email']['field'] = '<input name="user_email" type="text" id="user_email" value="" class="textbox" required="true" placeholder="*이메일">';
	$rows['user_email']['field_after'] = '</div>';


	// password
	$rows['password']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/password.png'));
	$rows['password']['field'] = '<input name="password" type="text" id="password" value="" class="textbox" required="true" placeholder="*비밀번호">';
	$rows['password']['field_after'] = '</div>';

	// firstname
	$rows['first_name']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/firstname.png'));
	$rows['first_name']['field'] = '<input name="first_name" type="text" id="first_name" value="" class="textbox" required="true" placeholder="*성함">';
	$rows['first_name']['field_after'] = '</div>';

	// phone
	$rows['billing_phone']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/phone.png'));
	$rows['billing_phone']['field'] = '<input name="billing_phone" type="text" id="billing_phone" value="" class="textbox" required="true" placeholder="*전화번호">';
	$rows['billing_phone']['field_after'] = '</div>';

	// address
	$rows['billing_address_1']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/home.png'));
	$rows['billing_address_1']['field'] = '<input name="billing_address_1" type="text" id="billing_address_1" value="" class="textbox" required="true" placeholder="*주소">';
	$rows['billing_address_1']['field_after'] = '</div>';

	// birthday
	$rows['birthday']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="div_date">
				<div class="field-icon-container">
					<img class="w-[16px]" src="%s" alt="">
				</div>
		', roum_get_image_url('login-icons/calendar.png'));
	$rows['birthday']['field'] = '<input name="birthday" type="date" placeholder="*생일" onclick="this.showPicker()" id="birthday" value="" class="textbox hasDatepicker" data-placeholder="*생년월일" required="">';
	$rows['birthday']['field_after'] = '</div></div>';

	// sex
	$rows['sex']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/sex.png'));
	$rows['sex']['field'] = '<input name="sex" type="text" id="sex" value="" class="textbox" required="true" placeholder="*성별">';
	$rows['sex']['field_after'] = '</div>';

	// group_name
	$rows['group_name']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/building.png'));
	$rows['group_name']['field'] = '<input name="group_name" type="text" id="group_name" value="" class="textbox" placeholder="단체명">';
	$rows['group_name']['field_after'] = '</div>';

	// team
	$rows['team_select']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/team.png'));
	$rows['team_select']['field_after'] = '</div>';

	// reg_level
	$rows['reg_levle']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/reward.png'));
	$rows['reg_levle']['field_after'] = '</div>';

	// recomend
	$rows['recomend']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/email.png'));
	$rows['recomend']['field'] = '<input name="recomend" type="text" id="recomend" value="" class="textbox" placeholder="추천인 아이디">';
	$rows['recomend']['field_after'] = '</div>';

	// ambassaord_reg
	$rows['ambassaord_reg']['field_before'] = sprintf('
		<div class="field-container" for="log">
			<div class="field-icon-container">
				<img class="w-[16px]" src="%s" alt="">
			</div>
		', roum_get_image_url('login-icons/id-card.png'));
	$rows['ambassaord_reg']['field'] = '<input name="ambassaord_reg" type="text" id="ambassaord_reg" value="" class="textbox" placeholder="담당엠베서더"	>';
	$rows['ambassaord_reg']['field_after'] = '</div>';

	// inter_society
	$rows['inter_society']['field_before'] = '<div class="field-container multi-select-field-container inter_society-field-container" for="log">';
	$inter_society_checkboxes = ['교육', '지역사회', '문화예술', '가정', '의료', '재난', '환경', '해외'];
	$inter_society_checkboxes_fields = "";
	foreach($inter_society_checkboxes as $checkbox) {
		$inter_society_checkboxes_fields .= sprintf('
			<div class="field-group">
				<input name="inter_society[]" type="checkbox" id="inter_society[%s]" value="%s">
				<label for="inter_society[%s]" id="inter_society[%s]" class="multicheckbox">%s</label>
			</div>',
			$checkbox, $checkbox, $checkbox, $checkbox, $checkbox
		);
	}
	$rows['inter_society']['field'] = $inter_society_checkboxes_fields;
	$rows['inter_society']['field_after'] = '</div>';

	// most_inter
	$rows['most_inter']['field_before'] = '<div class="field-container multi-select-field-container most_inter-field-container" for="log">';
	$most_inter_checkboxes = ['문화예술', '골프/레저/스포츠', '법률/경영', '고급주류', '미용/뷰티', '호텔/의전'];
	$most_inter_checkboxes_fields = "";
	foreach($most_inter_checkboxes as $checkbox) {
		$most_inter_checkboxes_fields .= sprintf('
			<div class="field-group">
				<input name="most_inter[]" type="checkbox" id="most_inter[%s]" value="%s">
				<label for="most_inter[%s]" id="most_inter[%s]" class="multicheckbox">%s</label>
			</div>',
			$checkbox, $checkbox, $checkbox, $checkbox, $checkbox
		);
	}
	$rows['most_inter']['field'] = $most_inter_checkboxes_fields;
	$rows['most_inter']['field_after'] = '</div>';

  return $rows;
}

add_filter('wpmem_register_form_args', function($args) {
	$option = get_cosmosfarm_members_option();
	if(!is_array($args)){
		$args = array();
	}
	$logo_path = roum_get_image_url('login-icons/logo_white.png');
	$args['main_div_before'] = sprintf('<div class="cosmosfarm-members-form signup-form %s">
	<div class="hero-container">
		<img decoding="async" src="%s">
	</div>', $option->skin, $logo_path);

	$args['fieldset_before'] = '<div class="empty-pan"></div>';

	return $args;
}, 100, 1);


// 회원가입에 성공했을시 돌아갈 페이지
add_action('template_redirect', function() {
	if (isset($_GET['register_success'])) {
		wp_redirect(home_url('/register-complete-thank-you')); // Change '/thank-you' to your desired URL
		exit;
	}
}, 10000);
add_shortcode( 'register_complete_thank_you', function() {
	ob_start(); ?>
	<script src="https://cdn.tailwindcss.com"></script>

	<div class="register-complete-thank-you ml-[-10px] mr-[-10px]">
		<div class="hero-container bg-[#483D3C] h-[30vh] w-full pb-[20px] flex justify-center items-center">
			<img class="w-[80px]" src="<?php echo roum_get_image_url('login-icons/logo_white.png'); ?>" alt="">
		</div>
		<div class="floated-box max-w-[80%] mx-auto shadow-xl -mt-12 z-50 bg-white p-5 relative lg:max-x-[60%]">
			<div class="inner flex flex-col justify-evenly h-[60vh]">
				<div class="empty-pan bg-[#F2CE54] absolute top-[-10px] h-[10px] w-[80%] translate-x-[-50%] left-[50%]"></div>
				<div class="flex justify-center">
					<img class="w-[120px]" src="<?php echo roum_get_image_url('login-icons/logo_transparent.png'); ?>" alt="">
				</div>
				<div class="center-message text-center">
					<div class="top text-black font-bold">가입신청이 완료되었습니다</div>
					<div class="bottom text-yellow-300">WE APPRECIATE YOUR APPLICATION</div>
				</div>
				<div class="bottom-message text-sm text-center">
					신청절차가 마무리 되면 기입해 주신 연락처로 추천 앰버서더가 연락을 드립니다. 가입까지 약 1-5일이 소요됩니다
				</div>
			</div>
			<div class="divider h-[1px] bg-gray-400 my-6"></div>
			<a href="/" class="text-center block py-4 bg-[#483D3C] !text-white !no-underline">메인 페이지</a>
		</div>
	</div>

	<?php
	return ob_get_clean();
} );
