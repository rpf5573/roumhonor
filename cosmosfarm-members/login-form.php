<?php if(!defined('ABSPATH')) exit;?>
<div class="cosmosfarm-members-form signin-form <?php echo $option->skin?>">
  <div class="hero-container">
    <img src="<?php echo roum_get_image_url('login-icons/logo_white.png'); ?>" alt="">
  </div>
	<form method="post" action="<?php echo esc_url($login_action_url)?>">
    <div class="empty-pan"></div>
		<input type="hidden" name="redirect_to" value="<?php echo esc_url($redirect_to)?>">
		<input type="hidden" name="a" value="login">

    <div class="inner">
      <div class="top">
        <fieldset>
          <div class="field-container" for="log">
            <div class="field-icon-container">
              <img class="w-[16px]" src="<?php echo roum_get_image_url('login-icons/email.png'); ?>" alt="">
            </div>
            <input type="text" name="log" id="log" class="username" placeholder="이메일">
          </div>

          <div class="field-container">
            <div class="field-icon-container">
              <img class="w-[16px]" src="<?php echo roum_get_image_url('login-icons/password.png'); ?>" alt="">
            </div>
            <input type="text" name="pwd" id="pwd" class="password" placeholder="비밀번호">
          </div>
        </fieldset>
      </div>
      <div class="bottom">
        <fieldset>
          <div class="button_div">
            <label><input name="rememberme" type="checkbox" id="rememberme" value="forever"<?php if($option->rememberme_checked):?> checked<?php endif?>><?php echo __('Keep me signed in', 'cosmosfarm-members')?></label>
            <input type="submit" class="buttons" value="<?php echo __('Log In', 'cosmosfarm-members')?>">
          </div>
        </fieldset>
      </div>
    </div>
	</form>

  <?php echo cosmosfarm_members_social_buttons(array('redirect_to'=>$redirect_to))?>
  <div class="meta">
    <?php if(get_cosmosfarm_members_profile_url()):?>
    <div class="link-text pwdreset">
      <a href="<?php echo add_query_arg(array('a'=>'pwdreset'), get_cosmosfarm_members_profile_url())?>"><?php echo __('Forgot Password', 'cosmosfarm-members')?></a>
    </div>
    <?php endif?>
    
    <?php if(wp_registration_url()):?>
    <div class="link-text register">
      <a href="<?php echo wp_registration_url()?>"><?php echo __('Register', 'cosmosfarm-members')?></a>
    </div>
    <?php endif?>
  </div>
</div>
